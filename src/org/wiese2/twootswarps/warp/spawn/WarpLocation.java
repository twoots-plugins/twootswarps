package org.wiese2.twootswarps.warp.spawn;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.wiese2.helper.Hashing;
import org.wiese2.helper.IStorageSerializable;

import java.util.HashMap;
import java.util.Map;

public class WarpLocation extends IStorageSerializable {
    private double x;
    private double y;
    private double z;

    private float pitch;
    private float yaw;

    private Location location;

    public WarpLocation() {
    }

    public WarpLocation(Location loc) {
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();

        pitch = loc.getPitch();
        yaw = loc.getYaw();
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("x", x);
        map.put("y", y);
        map.put("z", z);

        map.put("pitch", pitch);
        map.put("yaw", yaw);
        return map;
    }

    @Override
    public IStorageSerializable deserialize(Map<String, Object> map) {
        this.x = (Double) map.get("x");
        this.y = (Double) map.get("y");
        this.z = (Double) map.get("z");

        this.pitch = (Float) map.get("pitch");
        this.yaw = (Float) map.get("yaw");

        return this;
    }

    @Override
    public String getHash() {
        return Hashing.md5(this.x + "-" + this.y + "-" + this.z);
    }

    public boolean prepareWorld(String name, String world, Long seed) {
        Bukkit.getLogger().info("Preparing warp \"" + name + "\" [" + world + "]...");

        WorldCreator creator = new WorldCreator(world);
        creator.seed(seed);

        World w = Bukkit.getServer().createWorld(creator);

        if (w == null) {
            Bukkit.getLogger().warning("Failed to load warp \"" + name + "\" [" + world + "]");
            return false;
        }

        this.location = new Location(w, x, y, z, yaw, pitch);
        return true;
    }

    public Location getLocation() {
        return location;
    }
}
