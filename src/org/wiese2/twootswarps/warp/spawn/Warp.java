package org.wiese2.twootswarps.warp.spawn;

import org.bukkit.Location;
import org.bukkit.World;
import org.wiese2.helper.Hashing;
import org.wiese2.helper.IStorageSerializable;

import java.util.HashMap;
import java.util.Map;

public class Warp extends IStorageSerializable {
    private WarpLocation location;

    private String name;
    private String worldName;
    private Long worldSeed;

    public Warp(Location loc, String name) {
        this.location = new WarpLocation(loc);
        this.name = name;

        World world = loc.getWorld();

        if (world != null) {
            this.worldName = world.getName();
            this.worldSeed = world.getSeed();
        }
    }

    public Warp() {

    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("location", location.serialize());
        map.put("name", name);
        map.put("worldName", worldName);
        map.put("worldSeed", worldSeed);
        return map;
    }

    @Override
    public IStorageSerializable deserialize(Map<String, Object> map) {
        Warp warp = new Warp();
        warp.location = (WarpLocation) (new WarpLocation()).deserialize((Map<String, Object>) map.get("location"));
        warp.name = (String) map.get("name");
        warp.worldName = (String) map.get("worldName");
        warp.worldSeed = (Long) map.get("worldSeed");

        return warp;
    }

    @Override
    public String getHash() {
        return Hashing.md5(name);
    }

    public boolean prepareLocation() {
        return this.location.prepareWorld(this.name, this.worldName, this.worldSeed);
    }

    public Location getLocation() {
        return location.getLocation();
    }

    public void setLocation(Location location) {
        this.location = new WarpLocation(location);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorldName() {
        return worldName;
    }

    public Long getWorldSeed() {
        return worldSeed;
    }
}
