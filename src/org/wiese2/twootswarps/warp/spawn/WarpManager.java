package org.wiese2.twootswarps.warp.spawn;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.wiese2.helper.Console;
import org.wiese2.helper.Storage;
import org.wiese2.twootswarps.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WarpManager {
    public Storage<Warp> storageManager;
    public HashMap<String, Warp> warps;

    public WarpManager() {
        storageManager = new Storage<>(Main.instance, ".warp", Warp::new);

        warps = storageManager.loadMapFromFiles();
    }

    public boolean doWarp(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "You need to specify a warp name");
                return true;
            }

            Player player = (Player) sender;

            Location loc = player.getLocation();
            Warp warp = new Warp(loc, args[0]);

            warp = warps.get(warp.getHash());

            if (warp == null) {
                Console.sendError(sender, "That warp doesn't exist");
            } else {
                player.teleport(warp.getLocation());
                Main.doTeleportSound(player);
                Console.sendSuccess(sender, "You were teleported to "+warp.getName());
            }
        }
        return true;
    }

    public ArrayList<String> listWarps() {
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Warp> entry : warps.entrySet()) {
            Warp warp = entry.getValue();

            list.add(warp.getName());
        }
        return list;
    }

    public boolean doWarps(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (warps.size() == 0) {
                Console.sendError(sender, "There are no warps");
                return true;
            }

            TextComponent message = new TextComponent("Warps: ");
            message.setColor(ChatColor.AQUA);

            boolean first = true;
            for (Map.Entry<String, Warp> entry : warps.entrySet()) {
                Warp warp = entry.getValue();

                if (first) {
                    first = false;
                } else {
                    TextComponent comma = new TextComponent(", ");
                    comma.setColor(net.md_5.bungee.api.ChatColor.GRAY);
                    message.addExtra(comma);
                }

                TextComponent click = new TextComponent(warp.getName());
                click.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warp " + warp.getName()));
                click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Teleport to " + warp.getName())));
                message.addExtra(click);
            }

            player.spigot().sendMessage(message);
        }
        return true;
    }

    public boolean doSetWarp(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "You need to specify a warp name");
                return true;
            }

            Player player = (Player) sender;

            Location loc = player.getLocation();
            Warp warp = new Warp(loc, args[0]);

            warps.put(warp.getHash(), warp);

            Console.sendSuccess(sender, "Warp "+warp.getName()+" has been set");
        }
        return true;
    }

    public boolean doDelWarp(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "You need to specify a warp name");
                return true;
            }

            boolean isConfirmed = false;
            if (args.length > 1 && args[1].equalsIgnoreCase("confirm")) {
                isConfirmed = true;
            }

            Player player = (Player) sender;

            Location loc = player.getLocation();
            Warp warp = new Warp(loc, args[0]);

            if (!warps.containsKey(warp.getHash())) {
                Console.sendError(sender, "Warp "+warp.getName()+" does not exist");
                return true;
            }

            if (!isConfirmed) {
                TextComponent message = new TextComponent("Are you sure you want to delete warp '"+warp.getName()+"'? Type ");
                message.setColor(net.md_5.bungee.api.ChatColor.AQUA);

                TextComponent click = new TextComponent("/delwarp "+warp.getName()+" confirm");
                click.setColor(net.md_5.bungee.api.ChatColor.RED);
                click.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/delwarp "+warp.getName()+" confirm"));
                click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Confirm deletion")));
                message.addExtra(click);

                TextComponent message2 = new TextComponent(" to delete it.");
                message2.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                message.addExtra(message2);

                player.spigot().sendMessage(message);
                return true;
            }

            warps.remove(warp.getHash());
            storageManager.remove(warp);

            Console.sendSuccess(sender, "Warp "+warp.getName()+" has been removed");
        }
        return true;
    }

    public void onDisable() {
        storageManager.saveMapToFiles(this.warps);
    }
}
