package org.wiese2.twootswarps.spawn;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.wiese2.helper.Console;
import org.wiese2.helper.Storage;
import org.wiese2.twootswarps.Main;

import java.util.HashMap;

public class SpawnManager {
    public Storage<Spawn> storageManager;
    public HashMap<String, Spawn> spawns;

    public SpawnManager() {
        storageManager = new Storage<>(Main.instance, ".spawn", Spawn::new);

        spawns = storageManager.loadMapFromFiles();
    }

    public boolean doSpawn(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            Location loc = player.getLocation();
            Spawn spawn = new Spawn(loc);

            Spawn loaded = spawns.get(spawn.getHash());

            if (loaded == null) {
                player.teleport(loc.getWorld().getSpawnLocation());
                Console.sendSuccess(sender, "There is no spawn set for this world. You were teleported to the default spawn.");
            } else {
                player.teleport(loaded.getLocation());
                Console.sendSuccess(sender, "You were teleported to the spawn.");
            }
            Main.doTeleportSound(player);
        }
        return true;
    }

    public boolean doSetSpawn(CommandSender sender) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            Location loc = player.getLocation();
            Spawn spawn = new Spawn(loc);

            spawns.put(spawn.getHash(), spawn);

            loc.getWorld().setSpawnLocation(loc);

            Console.sendSuccess(sender, "New spawn for this world has been set.");
        }
        return true;
    }

    public void onDisable() {
        storageManager.saveMapToFiles(this.spawns);
    }
}
