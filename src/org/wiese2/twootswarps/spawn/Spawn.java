package org.wiese2.twootswarps.spawn;

import org.bukkit.Location;
import org.wiese2.helper.Hashing;
import org.wiese2.helper.IStorageSerializable;

import java.util.Map;

public class Spawn extends IStorageSerializable {
    private Location location;

    public Spawn(Location loc) {
        location = loc;
    }

    public Spawn() {

    }

    @Override
    public Map<String, Object> serialize() {
        return location.serialize();
    }

    @Override
    public IStorageSerializable deserialize(Map<String, Object> map) {
        Spawn spawn = new Spawn();
        spawn.location = Location.deserialize(map);

        return spawn;
    }

    @Override
    public String getHash() {
        return Hashing.md5("global");
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
