package org.wiese2.twootswarps;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.wiese2.helper.Console;
import org.wiese2.twootswarps.spawn.SpawnManager;
import org.wiese2.twootswarps.warp.spawn.Warp;
import org.wiese2.twootswarps.warp.spawn.WarpManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main extends JavaPlugin {
    public static JavaPlugin instance = null;
    public SpawnManager spawnManager;
    public WarpManager warpManager;

    @Override
    public void onEnable() {
        instance = this;
        spawnManager = new SpawnManager();
        warpManager = new WarpManager();

        for (Map.Entry<String, Warp> entry : warpManager.warps.entrySet()) {
            entry.getValue().prepareLocation();
        }
    }

    @Override
    public void onDisable() {
        spawnManager.onDisable();
        warpManager.onDisable();
    }

    @Override
    public ArrayList<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (isCommand(command, "spawn", "warps", "setspawn")) {
            return list();
        } else if (isCommand(command, "goto")) {
            if (args.length == 1) {
                ArrayList<String> worldNames = new ArrayList<>();
                List<World> worlds = Bukkit.getWorlds();

                for (World world : worlds) {
                    worldNames.add(world.getName());
                }

                return worldNames;
            } else if (args.length == 2) {
                return list("force");
            } else if (args.length == 3) {
                return list("seed");
            }
            return list();
        } else if (isCommand(command, "setwarp")) {
            if (args.length == 1) {
                return list("warp-name");
            }
            return list();
        } else if (isCommand(command, "warp", "delwarp")) {
            if (args.length == 1) {
                return warpManager.listWarps();
            } else if (args.length == 2 && isCommand(command, "delwarp")) {
                return list("confirm");
            }
            return list();
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (isCommand(command, "spawn")) {
            return spawnManager.doSpawn(sender);
        } else if (isCommand(command, "setspawn")) {
            return spawnManager.doSetSpawn(sender);
        }

        if (isCommand(command, "warp")) {
            return warpManager.doWarp(sender, args);
        } else if (isCommand(command, "warps")) {
            return warpManager.doWarps(sender);
        } else if (isCommand(command, "setwarp")) {
            return warpManager.doSetWarp(sender, args);
        } else if (isCommand(command, "delwarp")) {
            return warpManager.doDelWarp(sender, args);
        }

        if (isCommand(command, "goto")) {
            return this.doGoTo(sender, args);
        }
        return false;
    }

    private boolean doGoTo(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "You need to specify a warp name");
                return true;
            }
            String worldName = args[0];

            World world = Bukkit.getServer().getWorld(worldName);

            if (args.length >= 2 && world == null && "force".equals(args[1])) {
                WorldCreator creator = new WorldCreator(worldName);
                if (args.length >= 3) {
                    try {
                        creator.seed(Long.parseLong(args[2]));
                    } catch (NumberFormatException e) {
                        Console.sendError(sender, "Invalid seed");
                        return true;
                    }
                }

                Console.sendNotice(sender, "Loading world \"" + worldName + "\"...");
                world = Bukkit.getServer().createWorld(creator);
            }

            if (world == null) {
                Console.sendError(sender, "That world doesn't exist");
            } else {
                Player player = (Player) sender;

                Location loc = player.getLocation();
                loc.setWorld(world);

                player.teleport(loc);
                Main.doTeleportSound(player);
                Console.sendSuccess(sender, "You were teleported to the world " + world.getName());
            }
        }
        return true;
    }

    public static void doTeleportSound(Player player) {
        Location loc = player.getLocation();
        player.playSound(loc, Sound.ENTITY_ENDERMAN_TELEPORT, 0.3f, 1);
    }

    public boolean isCommand(Command command, String... compare) {
        for (String comp : compare) {
            if (command.getName().equalsIgnoreCase(comp)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<String> list(String... args) {
        return new ArrayList<>(Arrays.asList(args));
    }
}
