## TwootsWarps

Simple warps and spawn plugin.

### Usage

**Basic Commands**

- `/spawn`
    - Teleports you to spawn
    - If no spawn is set it will teleport you to the world spawn
- `/warp <name>`
    - Teleports you to a warp
- `/warps`
    - Lists all warps

**Admin Commands**

- `/setspawn`
    - Sets the spawn and the world spawn
- `/setwarp <name>`
    - Sets a warp
- `/delwarp <name>`
    - Deletes a warp
    
### Permissions

- `twootswarps.basic`
    - Gives you access to all basic commands
- `twootswarps.admin`
    - Gives you access to all admin commands
    - Defaults to op only
